"use strict";

var http = require("http");

var server = http.createServer(function (request, response) {
  var headers = JSON.stringify(request.headers, null, 2); // Stringify the headers with indentation for readability
  response.writeHead(200, { "Content-Type": "application/json" }); // Set Content-Type to application/json
  response.end(headers); // End the response with the stringified headers
});

server.listen(3000);

console.log("Server running at port 3000");
