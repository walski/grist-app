#!/bin/bash

set -eu

mkdir -p /app/data/storage
mkdir -p /app/data/oauth2-proxy
mkdir -p /app/data/grist

if [[ ! -f /app/data/env.sh ]]; then
    cp /app/pkg/env.sh.template /app/data/env.sh
fi

if [[ ! -f /app/data/oauth2-proxy/logo.png ]]; then
    cp /app/pkg/oauth2-proxy/logo.png /app/data/oauth2-proxy/logo.png
fi

source /app/data/env.sh

[[ ! -f /app/data/.secret_key ]] && openssl rand -base64 32 | tr -- '+/' '-_' > /app/data/.oauth2_proxy_cookie_secret
export OAUTH2_PROXY_COOKIE_SECRET=$(cat /app/data/.oauth2_proxy_cookie_secret)
export OAUTH2_PROXY_OIDC_ISSUER_URL=$CLOUDRON_OIDC_ISSUER
export OAUTH2_PROXY_CLIENT_ID=$CLOUDRON_OIDC_CLIENT_ID
export OAUTH2_PROXY_CLIENT_SECRET=$CLOUDRON_OIDC_CLIENT_SECRET

export GRIST_SANDBOX_FLAVOR=pyodide
export APP_HOME_URL=$CLOUDRON_APP_ORIGIN
export GRIST_HIDE_UI_ELEMENTS=helpCenter,billing,multiSite,multiAccounts
export GRIST_APP_ROOT=/app/code
export GRIST_DATA_DIR=/app/data/grist

# Required for forward auth
export GRIST_SINGLE_ORG=docs
export GRIST_FORWARD_AUTH_HEADER="x-forwarded-email"
export GRIST_FORWARD_AUTH_LOGOUT_PATH="/oauth2/sign_out"

export GRIST_DOMAIN=$CLOUDRON_APP_DOMAIN
export GRIST_HOST=localhost
export GRIST_IGNORE_SESSION=true
export GRIST_FORCE_LOGIN=true

export GRIST_SERVERS="home,docs,static,app"
export PORT=3000
export REDIS_URL="redis://default:$CLOUDRON_REDIS_PASSWORD@$CLOUDRON_REDIS_HOST:$CLOUDRON_REDIS_PORT"
export TYPEORM_TYPE=postgres
export TYPEORM_DATABASE=$CLOUDRON_POSTGRESQL_DATABASE
export TYPEORM_USERNAME=$CLOUDRON_POSTGRESQL_USERNAME
export TYPEORM_PASSWORD=$CLOUDRON_POSTGRESQL_PASSWORD
export TYPEORM_HOST=$CLOUDRON_POSTGRESQL_HOST
export TYPEORM_PORT=$CLOUDRON_POSTGRESQL_PORT


echo "=> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SupervisorExampleApp
