FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/oauth2-proxy
WORKDIR /app/code

# copy OAuth2 proxy files
ADD oauth2-proxy/* /app/code/oauth2-proxy/

# download OAuth2 proxy
RUN curl -L https://github.com/oauth2-proxy/oauth2-proxy/releases/download/v7.5.1/oauth2-proxy-v7.5.1.linux-amd64.tar.gz | tar -xzf - -C /app/code/oauth2-proxy --strip-components=1

# download Grist
RUN curl -L https://github.com/gristlabs/grist-core/archive/refs/tags/v1.1.4.tar.gz | tar -xzf - -C /app/code --strip-components=1

RUN apt -y update && apt -y install python3-venv

# install Grist
RUN yarn install

# Install pyodide for Grist sandboxing
RUN make -C ./sandbox/pyodide setup

RUN ./buildtools/prepare_python.sh

RUN yarn run build:prod
RUN yarn run install:python

# copy code
ADD start.sh /app/code/

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY env.sh.template start.sh /app/pkg/
COPY oauth2-proxy/logo.png /app/pkg/oauth2-proxy/

CMD [ "/app/code/start.sh" ]

